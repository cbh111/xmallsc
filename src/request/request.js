import axios from 'axios'
import {getStore} from '../utils/storage'

// 创建一个单例（实例）
const instance = axios.create({
  baseURL: 'http://localhost:3000',
  timeout: 5000
})

// 请求拦截
instance.interceptors.request.use(config => {
  const token = getStore('token')
  if (token) {
    // 表示用户已经登录
    config.headers.common['Authorization'] = token
  }
  return config
}, err => {
  return Promise.reject(err);
});

export default instance