import request from './request'

// 请求首页的数据
export const GetHomeData = () => request.get('/api/goods/home')

// 验证登录接口
export const IsLogin = (params) => request.post('/api/validate', params);

// 登录接口
export const GoLogin = (params) => request.post('/api/login', params);

// 添加购物车
export const AddCart = (params) => request.post('/api/addCart', params);

// 获取商品详情数据
export const GetGoodsDetail = (params) => request.get('/api/goods/productDet', {params})

// 获取购物车数据
export const GetCartList = (params) => request.post('/api/cartList', params);
