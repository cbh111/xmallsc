import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'

Vue.config.productionTip = false


// 使用vue-lazylocad
import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);

Vue.use(VueLazyload, {
  preLoad: 1,
  error: 'static/images/error.png',
  loading: 'static/images/load.gif',

})


// 挂载axios到vue的原型，由于继承性，所有的组件都可以使用this.$http
import axios from 'axios'
Vue.prototype.$http = axios
// 设置公共的url
axios.defaults.baseURL = 'http://localhost:3000';



new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

