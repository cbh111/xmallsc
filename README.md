# Xmall商城（PC端）



## 前言

本项目采用前后端分离开发，因为技术有限，后端没有使用数据库，全部使用json文件，网站设计来源github开源的Xmall商城项目。后端的全部代码在server文件夹下，安装好所有依赖后，使用node app运行程序。网站登录账号和密码可以随便输入，后端代码只设置了一个用户id。

如果对您有帮助，您可以点右上角“start”支持一下，谢谢！



## 项目展示

项目在线地址：http://39.105.157.163:8101



## 技术栈

vue + vuex + vue-router + element-ui + sass + axios + vue-lazyload



## 项目运行

#### 前端：

安装依赖

npm install

启动项目

npm run serve

项目打包

npm run build

#### 后端：

安装依赖

npm install

启动程序

node app

注意：默认程序启动在3000端口



## 主要组件

- Index.vue
- MHeader.vue
- Home.vue
- Shelf.vue
- MallGoods.vue
- BuyNum.vue
- Goods.vue
- GoodsDetail.vue
- Login.vue
- Cart.vue
- User.vue

## 部分截图展示

![截图展示](https://images.gitee.com/uploads/images/2021/0913/202644_01bf1e48_5695113.png "1.png")
![截图展示](https://images.gitee.com/uploads/images/2021/0913/202713_59d2f3fe_5695113.png "2.png")
![截图展示](https://images.gitee.com/uploads/images/2021/0913/202726_36a5b6b2_5695113.png "3.png")
![截图展示](https://images.gitee.com/uploads/images/2021/0913/202737_30b5005d_5695113.png "4.png")
![截图展示](https://images.gitee.com/uploads/images/2021/0913/202747_273e71b8_5695113.png "5.png")
![截图展示](https://images.gitee.com/uploads/images/2021/0913/202802_db3057ed_5695113.png "6.png")
![截图展示](https://images.gitee.com/uploads/images/2021/0913/202811_45be5b45_5695113.png "7.png")
![截图展示](https://images.gitee.com/uploads/images/2021/0913/202822_e68c1ecb_5695113.png "8.png")
![截图展示](https://images.gitee.com/uploads/images/2021/0913/202833_ce113f2c_5695113.png "9.png")
![截图展示](https://images.gitee.com/uploads/images/2021/0913/202844_e8c0434b_5695113.png "10.png")



## 结尾

感谢您的支持，如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！技术交流QQ：2678467517



